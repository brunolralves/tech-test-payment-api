namespace tech_test_payment_api.Enums
{
    public enum Status
    {
        Aguardando_Pagamento = 0,
        Pagamento_Aprovado = 1,
        Enviado_Para_Transportador = 2,
        Entregue = 3,
        Cancelada = 4
    }
}