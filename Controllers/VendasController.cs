using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Data;
using tech_test_payment_api.Enums;
using tech_test_payment_api.Functions;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendasDbContext _context;

        public VendasController(VendasDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Pedido pedido)
        {
          int contItens = 0;

          foreach(var item in pedido.Produtos)
          {
            contItens+=1;
          }  

          if(contItens < 1) return BadRequest("Pedido sem itens");
       

          pedido.StatusPagamento = Status.Aguardando_Pagamento;
          pedido.Data = DateTime.Now;

          _context.Vendedores.Add(pedido.Vendedor);

          decimal ValorTotal = 0;
          foreach(var item in pedido.Produtos)
          {
            if(item.PrecoUnitario <= 0 || item.Quantidade <= 0) return BadRequest("Quantidade ou preço não podem ser menores ou iguais a zero");
            
            ValorTotal += item.PrecoUnitario * item.Quantidade;
            _context.Produtos.Add(item);
          }

          pedido.ValorTotal = ValorTotal;
          _context.Pedidos.Add(pedido);
          _context.SaveChanges();

          return Ok(pedido);

        
        }   

        [HttpGet("{id}")]
        public IActionResult ObtervendaPorId(int id)
        {   
            var pedidoNoBanco = _context.Pedidos.FirstOrDefault(p => p.Id == id);

            if(pedidoNoBanco == null) return NotFound("Pedido não encontrado");
            
            pedidoNoBanco.Produtos = _context.Produtos.Where(p => p.PedidoId == id).ToList();
            pedidoNoBanco.Vendedor = _context.Vendedores.FirstOrDefault(p => p.Id == pedidoNoBanco.VendedorId);

            return Ok(pedidoNoBanco);
        }


        [HttpPut]
        public ActionResult AtualizarStatus(int idPedido,Status novoStatus)
        {
            var pedidoNoBanco = _context.Pedidos.FirstOrDefault(p => p.Id == idPedido);

            if(pedidoNoBanco == null) return NotFound("Pedido não encontrado");

            var statusValidado = Validadores.ValidarStatus(pedidoNoBanco.StatusPagamento,novoStatus);
            
            if(statusValidado)
            {
                pedidoNoBanco.StatusPagamento = novoStatus;
                _context.SaveChanges();
                return Ok("Status atualizado com sucesso");
            }
            else
            {
                return BadRequest("Status inválido");
            }

        }
    }
}