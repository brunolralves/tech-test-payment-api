using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Pedido
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Produto> Produtos { get; set; }
        public Status StatusPagamento { get; set; }
        public decimal ValorTotal { get; set; }
        public DateTime Data { get; set; }
    }
}