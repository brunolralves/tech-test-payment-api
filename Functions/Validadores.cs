using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Functions
{
    public class Validadores
    {
        public static bool ValidarStatus(Status statusAtual,Status statusNovo)
        {
           
            switch (statusAtual)
            {
                /*
                De: Aguardando pagamento Para: Pagamento Aprovado 0 -> 1
                De: Aguardando pagamento Para: Cancelada 0 -> 4
                */
                case Status.Aguardando_Pagamento:
                   return (statusNovo == Status.Pagamento_Aprovado || statusNovo == Status.Cancelada) ?  true :  false;
                                       
                break;

                /*
                De: Pagamento Aprovado Para: Enviado para Transportadora 1 -> 2
                De: Pagamento Aprovado Para: Cancelada 1 -> 4
                */
                case Status.Pagamento_Aprovado:
                   return (statusNovo == Status.Enviado_Para_Transportador || statusNovo == Status.Cancelada)?  true :  false;
                    
                break;

                //De: Enviado para Transportador. Para: Entregue 2 -> 3
                case Status.Enviado_Para_Transportador:
                    return (statusNovo == Status.Entregue) ?  true :  false;                  
                break;

                default:
                    return false;
            }

        }
    }
}